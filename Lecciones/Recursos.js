import React, { useState } from 'react';
import { Text, TouchableOpacity, Image, View, FlatList, StyleSheet } from 'react-native';
import { apiUrl } from '../helpers/api';
import * as WebBrowser from 'expo-web-browser';

export default function Recursos(props) {

    const [recursos, setRecursos] = useState(props.recursos)

    return (
        <View style={{ flex: 1, justifyContent: 'center', borderRadius: 15, backgroundColor: 'white', margin: 10 }}>
            <Text style={{ padding: 15 }}>
                SI DESEAS APRENDER MAS DE FORMA RAPIDA Y DIVERTIDA DESCARGA LOS SIGUIENTES RECURSOS
            </Text>
            <FlatList
                data={recursos}
                keyExtractor={(_, i) => `${i}`}
                scrollEventThrottle={32}
                pagingEnabled
                style={{ flexGrow: 1, }}
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity style={styles.center}
                            onPress={() => WebBrowser.openBrowserAsync(apiUrl + item.url)}>
                            <Text style={{ padding: 15, }} > {item.name}</Text>
                            <Image style={{ width: 20, height: 20 }} tintColor="blue"
                                source={require('../assets/archivo.png')} />
                        </TouchableOpacity>
                    );
                }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
});