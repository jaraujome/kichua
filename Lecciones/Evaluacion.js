import React, { useState } from 'react';
import { Text, TextInput, TouchableOpacity, Image, View, Alert, FlatList, StyleSheet} from 'react-native';
import { updateLeccionInstance, apiUrl } from '../helpers/api';
import BouncyCheckbox from "react-native-bouncy-checkbox";


export default function Evaluacion(props) {

    const [leccion, setLeccion] = useState(props.leccion)
    const [isLast, setIsLast] = useState(props.isLast)

    function updateChanges() {

        updateLeccionInstance(leccion)
            .then(function (response) {
                console.log('update leccionInstance ', response._id);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    // compara la respuesta con la respuestaCorrecta
    function isValid(pregunta) {
        const removeAccents = (str) => {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }
        return removeAccents(pregunta.respuesta.toLowerCase().trim()) === removeAccents(pregunta.respuestaCorrecta.toLowerCase().trim());
    }

    function evaluar() {
        const findRespuestaEmpty = leccion.evaluacion.find(p => p.respuesta == '');

        if (findRespuestaEmpty) {
            Alert.alert('Complete la evaluación', 'Preguntas sin responder');
        } else {
            let newLeccion = JSON.parse(JSON.stringify(leccion));
            let nota = 0;
            newLeccion.evaluacion.forEach(pregunta => {
                if (isValid(pregunta)) {
                    nota += pregunta.nota;
                }
            });
            newLeccion.nota = nota;
            if (nota >= 14) {
                newLeccion.status = 'Completado';
                newLeccion = JSON.parse(JSON.stringify(newLeccion));
                setLeccion(newLeccion);
                updateChanges();
                Alert.alert('¡Felicitaciones!', 'Ha completado la lección su nota es de ' + newLeccion.nota + ' puntos.');
                if (isLast) {
                    fetch(apiUrl + 'leccioninstance/certificado/' + leccion.user);
                }
            } else {
                Alert.alert('Intenta de nuevo', 'Su nota es de ' + nota + ' puntos se necesita un minimo de 14 puntos.');
            }
        }
    }

    function changeRespuesta(value, pregunta) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        const index = newLeccion.evaluacion.findIndex( p => p.numero == pregunta.numero);
        newLeccion.evaluacion[index].respuesta = value;
        setLeccion(newLeccion);
    }

    // Toggle en preguntas Verdadero y Falso (cuestionario)
    function toggleRepuesta(value, pregunta) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        const index = newLeccion.evaluacion.findIndex( p => p.numero == pregunta.numero);
        // las respuestas de tipo cuestionario se manejan con strings que indiquen true o false
        newLeccion.evaluacion[index].respuesta = value ? 'true' : 'false';
        setLeccion(newLeccion);
        return newLeccion.evaluacion[index].respuesta
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>

            <FlatList
                data={leccion.evaluacion.sort((a, b) => (b.tipo == 'Cuestionario' && a.tipo == 'Completación') ? -1 : 1)}
                keyExtractor={item => `${item.numero}`}
                style={{ flexGrow: 1 }}
                contentContainerStyle={{
                    paddingBottom: !leccion.evaluacion.filter(p => p.tipo == 'Cuestionario').length ? 200 : 100
                }}
                renderItem={({ item, index }) => {
                    return (
                        <>

                            { leccion.status != 'Completado' && !index && item.tipo == 'Completación' &&(
                                <Text style={{ padding: 15, }} >
                                    En las siguientes preguntas complete con el pronombre o articulo que corresponda.
                                </Text>
                            )}

                            {leccion.status == 'Completado' && (
                                <Image style={styles.iconInfo}
                                    tintColor={isValid(item) ? "green" : "red"} 
                                    source={isValid(item) ? require('../assets/checked.png') : require('../assets/close.png')} 
                                />
                            )}
                            
                            {/* Si es la primera tipo cuestionario muestra titulo de preguntas de cuestionario */}
                            {leccion.status != 'Completado' && item.tipo !== 'Completación' &&
                                    (!index || (leccion.evaluacion[index-1].tipo === 'Completación') ) && (
                                <Text style={{ padding: 15 }}>
                                    En las siguientes preguntas escoja si la afirmación es verdadera o falsa.
                                </Text>
                            )}

                            {item.tipo !== 'Completación' && (
                                <>
                                    <Text style={{ padding: 15, paddingLeft: 35 }} > {(index + 1) + ') '} {item.texto}</Text>
                                    <View style={{ paddingBottom: 15, justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center' }}>
                                        <BouncyCheckbox
                                            size={20}
                                            fillColor="blue"
                                            unfillColor="#FFFFFF"
                                            text='V'
                                            disabled={leccion.status == 'Completado'}
                                            disableBuiltInState
                                            textStyle={{ textDecorationLine: 'none' }}
                                            iconStyle={{ borderColor: "blue" }}
                                            isChecked={(item.respuesta === 'true')}
                                            onPress={(value) => toggleRepuesta(!value, item)}
                                        />
                                        <BouncyCheckbox
                                            size={20}
                                            fillColor="red"
                                            unfillColor="#FFFFFF"
                                            text='F'
                                            disabled={leccion.status == 'Completado'}
                                            disableBuiltInState
                                            textStyle={{ textDecorationLine: 'none' }}
                                            iconStyle={{ borderColor: "red" }}
                                            isChecked={(item.respuesta === 'false')}
                                            onPress={(value) => toggleRepuesta(value, item)}
                                        />
                                    </View>
                                </>
                            )}

                            {item.tipo === 'Completación' && (
                                <>
                                    <Text style={{ padding: 15, paddingLeft: 35 }}>
                                        {(index + 1) + ') '}
                                        {
                                            item.texto.replace(item.respuestaCorrecta, '___________')
                                        }
                                    </Text>
                                    <TextInput
                                        placeholder="Respuesta"
                                        editable={leccion.status != 'Completado'}
                                        value={item.respuesta}
                                        style={{ paddingHorizontal: 35 }}
                                        onChangeText={text =>
                                            changeRespuesta(text, item)
                                        }
                                    />
                                </>
                            )}

                        </>
                    );
                }}
            />

            {leccion.status != 'Completado' && (
                <TouchableOpacity
                    onPress={() => evaluar()}>
                    <View style={styles.chip}>
                        <Text style={{ width: 'auto', paddingVertical: 8}} > Enviar respuestas </Text>
                        <Image
                            tintColor="blue"
                            style={{ width: 14, height: 14 }}
                            source={require('../assets/send.png')}
                        />
                    </View>
                </TouchableOpacity>
            )}

            {leccion.status == 'Completado' && (
                <View style={styles.chip}>
                    <Text style={{ width: 'auto', paddingVertical: 5 }} > Nota: {leccion.nota} </Text>
                </View>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    iconInfo: {
        position: 'absolute',
        top: 30,
        left: 10,
        height: 16,
        width: 16,
    },
    chip:{
        paddingHorizontal: 10,
        paddingVertical:0,
        justifyContent: 'flex-end',
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 15,
        borderColor: 'blue',
        display:'flex',
        alignItems: 'center',
        width:'auto',
        marginLeft:'auto',
        marginBottom: 5,
        marginRight: 5,
    }
});