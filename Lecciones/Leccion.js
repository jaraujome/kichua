import React, { useState, useEffect } from 'react';
import { Image } from 'react-native';
import { getLeccionInstance } from '../helpers/api';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Contenido from './Contenido';
import Evaluacion from './Evaluacion';
import Actividad from './Actividad';
import Recursos from './Recursos';

export default function Leccion({ route, navigation }) {

  const Tab = createBottomTabNavigator();

  const { leccionId, total } = route.params;

  const [leccion, setLeccion] = useState({ niveles: [], evaluacion: [], recursos: [] })

  useEffect(() => {
    getLeccionInstance(leccionId)
      .then(function (response) {
        setLeccion(response);
      })
      .catch(function (error) {
        //console.log(error);
      });
  }, [])

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconSrc = require('../assets/file.png');

          if (route.name === 'Contenido') {
            iconSrc = require('../assets/open-book.png');
          } else if (route.name === 'Recursos') {
            iconSrc = require('../assets/folder.png');
          } else if (route.name === 'Evaluacion') {
            iconSrc = require('../assets/checklist.png');
          }
          return <Image source={iconSrc} fadeDuration={0}
            tintColor={color}
            style={{ marginTop:5, width: 20, height: 20 }} />
        },
      })}
      tabBarOptions={{
        activeTintColor:'#fdd203',
        inactiveTintColor:'white',
        keyboardHidesTabBar: true,
        labelStyle:{
          fontSize: 14,
          textAlign:'center'
        },
        style: {
          backgroundColor: '#4e95c2',
        },
      }}
    >
      <Tab.Screen name="Actividad" children={() => <Actividad actividad={leccion.actividad} />} />
      <Tab.Screen name="Contenido" children={() => <Contenido niveles={leccion.niveles} />} />
      <Tab.Screen name="Recursos" children={() => <Recursos recursos={leccion.recursos} />} />
      <Tab.Screen name="Evaluacion" children={() => <Evaluacion leccion={leccion} isLast={leccion.numero == total} />} />
    </Tab.Navigator>
  );
}
