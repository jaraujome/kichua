import React, { useState } from 'react';
import { Animated, Text, Image, View, ScrollView, StyleSheet, Dimensions } from 'react-native';
import HTMLView from 'react-native-htmlview';

const { width, height } = Dimensions.get('screen');

const Backdrop = () => {
    return (
        <Animated.View
            style={[
                StyleSheet.absoluteFillObject, {
                    backgroundColor: '#1488CC',
                },
            ]}
        />
    )
}

export default function Actividad(props) {

    const htmlContent = props.actividad;

    return (
        <>
            <Backdrop />
            <ScrollView style={{
                width: width - 20, margin: 10,
                paddingTop: 15,
                paddingHorizontal: 15,
                borderRadius: 25, backgroundColor: 'white'
            }}>

                <HTMLView
                    value={htmlContent}
                    stylesheet={styles}
                />
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    p: {
        padding: 0,
        margin: 0,
    },
    ul: {
        padding: 0,
        margin: 0,
    },
});