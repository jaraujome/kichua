import React from 'react';
import { Animated, Text, View, ScrollView, StyleSheet, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

const { width, height } = Dimensions.get('screen');

const Backdrop = () => {
    return (
        <Animated.View
            style={[
                StyleSheet.absoluteFillObject, {
                    backgroundColor: '#2B32B2',
                },
            ]}
        />
    )
}

export default function Contenido(props) {

    const niveles = props.niveles;
    const scrollX = React.useRef(new Animated.Value(0)).current;

    const Indicator = ({ scrollX }) => {
        return (
            <View style={{ position: 'absolute', bottom: 10, flexDirection: 'row' }}>
                {niveles.map((_, i) => {
                    const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
                    const scale = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.8, 1.4, 0.8],
                        extrapolate: 'clamp',
                    });
                    const opacity = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.4, 1.4, 0.4],
                        extrapolate: 'clamp',
                    });
                    return (<Animated.View
                        key={`indicator-${i}`}
                        style={{
                            height: 10, width: 10, borderRadius: 5, backgroundColor: '#fdd203', opacity, margin: 10,
                            transform: [{ scale }]
                        }}
                    />
                    );
                })}
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <Backdrop />
            <Animated.FlatList
                data={niveles}
                keyExtractor={item => `${item.numero}`}
                horizontal
                scrollEventThrottle={32}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                    { useNativeDriver: false }
                )}
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                renderItem={({ item }) => {
                    return (
                        <View style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ padding: 10, color: '#fdd203' }} > Nivel {item.numero}</Text>
                            <Text style={{ padding: 5, color: '#fdd203' }}>{item.nombre}</Text>
                            <ScrollView style={{ width, marginTop: 10, marginBottom: 50, paddingHorizontal: 10, borderRadius: 25 }}>
                                <LinearGradient
                                    colors={['#fff', '#ccfcfc']}
                                    style={styles.container}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}>
                                    {
                                        item.diccionario.map((word, j) => (
                                            <View style={[styles.fields,
                                            { borderWidth: (j == 0 || j == (item.diccionario.length - 1)) ? 0 : 1 }
                                            ]} key={`word-${j}`}>
                                                <Text style={styles.space}>{word.es}</Text>
                                                <Text style={styles.space}>{word.kichua}</Text>
                                            </View>
                                        ))
                                    }
                                </LinearGradient>
                            </ScrollView>
                        </View>
                    );
                }}
            />
            <Indicator scrollX={scrollX} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
    },
    fields: {
        display: 'flex',
        flexDirection: 'row',
        // borderWidth: 1,
        borderColor: "black"
    },
    space: {
        width: (width / 2),
        padding: 10,
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: 'black',
        alignSelf: 'center'
    },
});
