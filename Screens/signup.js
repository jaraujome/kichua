import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Alert } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { signUp } from '../helpers/api'

export default function Signin(props) {

  const [username, setUsername] = React.useState('');
  const [name, setName] = React.useState('');
  const [lastName, setLastName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  const handleSignUp = () => {
    if (username && email && password && name && lastName) {
      if(!validateEmail(email)){
        Alert.alert('Error:', 'Email invalido');
        return;
      }

      const infoUser = {
        username,
        email,
        password,
        name,
        lastName,
      }
      signUp(infoUser)
        .then(function (res) {
          if (res && res.token) {
            Alert.alert('Estimado Usuario:', 'Usted se ha registrado exitosamente en nuestro sistema, por favor a traves de la flecha de arriba dirijase al pantalla de Login, para que disfrute de su experiencia.')
            setUsername('')
            setName('')
            setLastName('')
            setEmail('')
            setPassword('')
          } else {
            Alert.alert('Error: ', res.message ? res.message : '')
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    } else{
      Alert.alert('Error:', 'Todos los campos son requeridos');
    }
  }

  return (
    <LinearGradient
      colors={['#1488CC', '#2B32B2']}
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <Image source={require('../assets/login1.png')}
        style={{
          width: '60%',
          height: '30%',
          marginTop: 0
        }}
      />
      <Text
        style={{
          fontSize: 30,
          alignSelf: "center",
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: 'bold',
          color: '#FFFEFE',
        }}
      >Kichua-Facil</Text>
      <Text
        style={{
          marginHorizontal: 55,
          textAlign: 'center',
          marginTop: 5,
          color: '#FFFEFE'
        }}
      >
        Entre a nuestra plataforma para interactuar
        con contenido seguro.
      </Text>
      <View
        style={{
          alignItems: "center",
          marginHorizontal: 20,
          borderWidth: 2,
          marginTop: 20,
          paddingHorizontal: 50,
          borderColor: "#FFFEFE",
          borderRadius: 23,
          paddingVertical: 2
        }}>
        <TextInput
          placeholder='Usuario'
          placeholderTextColor='white'
          style={{ paddingHorizontal: 10 }}
          value={username}
          onChangeText={(username) => setUsername(username)}
        />
      </View>

      <View
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 10,
          minHeight: 28,
          flexDirection: 'row'
        }}>
        <TextInput
          placeholder='Nombre'
          placeholderTextColor='white'
          style={{
            alignItems: "center",
            marginHorizontal: 20,
            borderWidth: 2,
            paddingHorizontal: 20,
            borderColor: "#FFFEFE",
            borderRadius: 23,
            paddingVertical: 2
          }}
          value={name}
          onChangeText={(name) => setName(name)}
        />
        <TextInput
          placeholder='Apellido '
          placeholderTextColor='white'
          style={{
            alignItems: "center",
            marginHorizontal: 20,
            borderWidth: 2,
            paddingHorizontal: 20,
            borderColor: "#FFFEFE",
            borderRadius: 23,
            paddingVertical: 2
          }}
          value={lastName}
          onChangeText={(lastName) => setLastName(lastName)}
        />
      </View>

      <View
        style={{
          alignItems: "center",
          marginHorizontal: 20,
          marginTop: 10,
          borderWidth: 2,
          paddingHorizontal: 70,
          borderColor: "#FFFEFE",
          borderRadius: 23,
          paddingVertical: 2
        }}>
        <TextInput
          placeholder='Correo Electronico'
          placeholderTextColor='white'
          style={{ paddingHorizontal: 20 }}
          value={email}
          onChangeText={(email) => setEmail(email)}
        />
      </View>

      <View
        style={{
          alignItems: "center",
          marginHorizontal: 20,
          borderWidth: 2,
          marginTop: 20,
          paddingHorizontal: 30,
          borderColor: "#FFFEFE",
          borderRadius: 23,
          paddingVertical: 2
        }}>
        <TextInput
          secureTextEntry={true}
          placeholder='Contraseña'
          placeholderTextColor='white'
          style={{ paddingHorizontal: 10 }}
          value={password}
          onChangeText={(password) => setPassword(password)}
        />
      </View>

      <TouchableOpacity onPress={handleSignUp}>
        <View
          style={{
            marginHorizontal: 55,
            alignItems: "center",
            marginTop: 20,
            marginBottom: 20,
            paddingHorizontal: 50,
            backgroundColor: "#c69723",
            paddingVertical: 10,
            borderRadius: 25
          }}
        >
          <Text style={{ color: "white" }}>Registrar</Text>
        </View>
      </TouchableOpacity>

    </LinearGradient>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
