import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TextInput, View, Image, ToastAndroid, FlatList, Alert, TouchableOpacity, ActivityIndicator, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { FlatGrid } from 'react-native-super-grid';
import { getLeccions, apiUrl, updateLeccion, addLeccion, removeLeccion } from '../helpers/api';
const { width, height } = Dimensions.get('screen');

export default function Configuracion({ navigation }) {

  const [items, setItems] = useState([]);
  const [flatListRef, setFlatListRef] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    obtenerLecciones();
  }, [])

  function obtenerLecciones() {
    getLeccions()
      .then(function (response) {
        console.log(response.length);
        setItems(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  function changeNameLeccion(nombre, leccion) {
    leccion.nombre = nombre;
    setLoading(true)
    updateLeccion(leccion)
      .then(function (response) {
        setLoading(false)
        ToastAndroid.show('Cambios Guardados!', ToastAndroid.SHORT);
      })
      .catch(function (error) {
        console.log(error);
        setLoading(false)
        ToastAndroid.show('Error al guardar!', ToastAndroid.SHORT);
      });
  }

  function agregarLeccion() {

    setLoading(true)

    const newLeccion = {
      numero: (items.length + 1),
      nombre: 'lección ' + (items.length + 1),
      actividad: 'actividad',
      recursos: [],
      niveles: [
        {
          numero: 1,
          nombre: 'nivel 1',
          diccionario: []
        }
      ],
      evaluacion: []
    }

    addLeccion(newLeccion)
      .then(function (response) {
        console.log('leccion agregada, update items');
        setLoading(false)
        ToastAndroid.show('Lección Agregada!', ToastAndroid.SHORT);
        obtenerLecciones();
      })
      .catch(function (error) {
        console.log(error);
        setLoading(false)
        ToastAndroid.show('Error al guardar!', ToastAndroid.SHORT);
      });
  }

  function deleteLeccion(leccion) {
    setLoading(true)
    removeLeccion(leccion._id)
      .then(function (response) {
        setLoading(false)
        ToastAndroid.show('Lección Eliminada!', ToastAndroid.SHORT);
        obtenerLecciones();
      })
      .catch(function (error) {
        console.log(error);
        setLoading(false)
        ToastAndroid.show('Error al eliminar!', ToastAndroid.SHORT);
      });
  }

  const confirmDelete = (leccion) =>
    Alert.alert(
      "Eliminar lección",
      "¿Seguro que desea eliminar la lección?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => deleteLeccion(leccion) }
      ]
    );

  return (
    <LinearGradient
      colors={['#1488CC', '#2B32B2']}
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >

      {loading && <View style={styles.spinner} pointerEvents={'none'}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>}

      <FlatList
        data={items}
        ref={ref => setFlatListRef(ref)}
        keyExtractor={item => `${item.numero}`}
        onContentSizeChange={() => { /*flatListRef.scrollToEnd({animated:true})*/ }}
        contentContainerStyle={{ paddingBottom: 150 }}
        style={styles.gridView}
        renderItem={({ item }) => (
          <View style={[styles.itemContainer, { backgroundColor: '#072b9d' }]}>
            <TouchableOpacity style={styles.iconOptions}
              onPress={() => navigation.navigate('LeccionAdmin', { leccionId: item._id, numero: item.numero })}>
              <Image style={{ height: 30, width: 30 }} tintColor="white" source={require('../assets/more.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.iconRemove}
              onPress={() => confirmDelete(item)}>
              <Image style={{ height: 20, width: 18 }} tintColor="white" source={require('../assets/trash.png')} />
            </TouchableOpacity>
            <Image style={styles.icon} source={{ uri: apiUrl + 'images/lecciones/leccion-' + item.numero + '.png' }} />
            <Text style={styles.itemName}>Lección {item.numero}</Text>
            <TextInput
              style={styles.itemCode}
              onEndEditing={(e) => changeNameLeccion(e.nativeEvent.text, item)}>
              {item.nombre}
            </TextInput>
          </View>
        )}
      />

      <TouchableOpacity style={{ position: 'absolute', bottom: 15, right: 10 }}
        onPress={() => agregarLeccion()}>
        <View style={{ justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
          <Text style={{ color: 'white' }} > Agregar Lección </Text>
          <Image style={{ width: 18, height: 18 }} tintColor={'white'} source={require('../assets/add.png')} />
        </View>
      </TouchableOpacity>

    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  gridView: {
    marginTop: 10,
    flex: 1,
    marginBottom: 50
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 7,
    padding: 10,
    height: 150,
    margin: 10,
    top: 0
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'white'
  },
  icon: {
    position: 'absolute',
    left: 20,
    top: 20,
    height: 40,
    width: 40,
  },
  iconOptions: {
    alignSelf: 'flex-end',
    right: 10,
    top: 20,
    position: 'absolute',
  },
  spinner: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    backgroundColor: 'black',
    width,
    height: height - 30,
    opacity: 0.6,
    zIndex: 1,
  },
  iconRemove: {
    alignSelf: 'flex-end',
    right: 10,
    top: 70,
    position: 'absolute',
  }
});
