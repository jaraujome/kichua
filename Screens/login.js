import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ActivityIndicator, Alert, Dimensions } from 'react-native'
import { LinearGradient } from 'expo-linear-gradient';
import { signIn } from '../helpers/api';

const { width, height } = Dimensions.get('screen');

export default function Login({ navigation }) {

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [loading, setLoading] = React.useState(false);

  const handleLogin = () => {
    if (username && password) {
      setLoading(true)
      signIn(username, password)
        .then(function (res) {
          if (res && res.token) {
            navigation.navigate('Inicio');
            setUsername('')
            setPassword('')
          } else {
            Alert.alert('Error: ', res.message ? res.message : '')
          }
          setLoading(false)
        })
        .catch(function (error) {
          Alert.alert('Estimado Usuario:', 'Si esta en presencia de esta pantalla , entonces el Usuario que escribe no existe o la Contraseña es incorrecta. Por Favor intente nuevamente. ')
          setLoading(false)
        });
    } else {
      Alert.alert('Error: ', 'Ingrese el usuario y la clave');
    }
  }

  const SignUp = () => {
    navigation.navigate('SignUp');
  }

  return (
    <LinearGradient
      colors={['#1488CC', '#2B32B2']}
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >

      {loading && <View style={styles.spinner} pointerEvents={'none'}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>}

      <Image source={require('../assets/login1.png')}
        style={{
          width: '60%',
          height: '40%',
          marginTop: -60
        }}
      />
      <Text
        style={{
          fontSize: 30,
          alignSelf: "center",
          fontFamily: 'Roboto',
          fontStyle: 'normal',
          fontWeight: 'bold',
          color: '#FFFEFE',
        }}
      >Kichua-Facil</Text>
      <Text
        style={{
          marginHorizontal: 55,
          textAlign: 'center',
          marginTop: 5,
          color: '#FFFEFE'
        }}
      >
        Entre a nuestra plataforma para interactuar
        con contenido seguro.
      </Text>

      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          alignSelf: 'stretch',
          justifyContent: 'center',
          marginHorizontal: 55,
          borderWidth: 2,
          marginTop: 40,
          paddingHorizontal: 10,
          borderColor: "#FFFEFE",
          borderRadius: 23,
          paddingVertical: 2
        }}>

        <TextInput
          placeholder='Correo Electronico'
          placeholderTextColor='white'
          style={{ paddingHorizontal: 10 }}
          value={username}
          onChangeText={(username) => setUsername(username)}
        />

      </View>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          marginHorizontal: 55,
          borderWidth: 2,
          marginTop: 20,
          paddingHorizontal: 30,
          borderColor: "#FFFEFE",
          borderRadius: 23,
          paddingVertical: 2
        }}>
        <TextInput
          inlineImageLeft='search_icon'
          placeholder='Contraseña'
          secureTextEntry={true}
          placeholderTextColor='white'
          value={password}
          style={{ paddingHorizontal: 10 }}
          onChangeText={(password) => setPassword(password)}

        />
      </View>
      <TouchableOpacity onPress={handleLogin}>
        <View
          style={{
            marginHorizontal: 55,
            alignItems: "center",
            marginTop: 30,
            paddingHorizontal: 50,
            backgroundColor: "#c69723",
            paddingVertical: 10,
            borderRadius: 25
          }}
        >
          <Text style={{ color: "white" }}>Entrar</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={SignUp}>
        <View
          style={{
            marginHorizontal: 55,
            marginTop: 15,
            paddingHorizontal: 20,
            backgroundColor: "#c69723",
            paddingVertical: 12,
            borderRadius: 25
          }}
        >
          <Text

            style={{ color: "white" }}
          >
            Desea crear su Cuenta ?</Text>
        </View>
      </TouchableOpacity>

    </LinearGradient>

  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spinner: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    backgroundColor: 'black',
    width,
    height: height - 30,
    opacity: 0.6,
    zIndex: 1,
  },
});
