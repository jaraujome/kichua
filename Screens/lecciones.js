import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { FlatGrid } from 'react-native-super-grid';
import { apiUrl, authService, getLeccionsUser, getLeccions } from '../helpers/api';
import * as WebBrowser from 'expo-web-browser';

export default function Lecciones({ navigation }) {

  const [items, setItems] = useState([]);
  const [leccions, setLeccions] = useState([]);
  const [currenLeccion, setCurrenLeccion] = useState(1);
  const [currentUser, setCurrentUser] = useState({});

  const colores = [
    '#072b9d', '#ffdd77', '#de7878', '#51c2d4', '#6e6fd4',
    '#e8d54a', '#ff6961', '#77dd77', '#fd961c', '#ff6565',
    '#75f9f2', '#ffdd77', '#de7878', '#b186f1', '#e8d54a',
  ];

  useEffect(() => {
    // para hacer las llamadas a la api cada vez que se enfoque la vista y poder mostrar la data real
    const willFocusSubscription = navigation.addListener('focus', () => {
      const userAuth = authService.currentUserValue;
      setCurrentUser(userAuth)
      getLeccionsUser(userAuth._id)
        .then(function (response) {
          setItems(response);
          const current = response.findIndex(l => l.status === 'En proceso') + 1;
          setCurrenLeccion(current);
          getLeccions()
            .then(function (lecciones) {
              setLeccions(lecciones.filter(l => l.numero > current));
            })
            .catch(function (error) {
              console.log(error);
            })
        })
        .catch(function (error) {
          console.log(error);
        })
    });
    return willFocusSubscription;

  }, []);

  const renderItem = ({ item }) => (
    <TouchableOpacity
      disabled={(!item.status)}
      onPress={() => navigation.navigate('Leccion', { leccionId: item._id, numero: item.numero, total: items.length + leccions })}>

      <View
        style={[styles.itemContainer,
        { backgroundColor: 'white', opacity: item.status ? 1 : 0.7 }
        ]}>

        <ImageBackground source={require('../assets/backleccion.png')} borderRadius={15}

          tintColor={colores[item.numero]} resizeMode="cover" style={styles.image}>

          <Text style={[styles.itemName, { color: colores[item.numero], marginBottom: 35 }]}>
            Lección {item.numero}
          </Text>

          {item.niveles.filter((_, i) => i < 3).map((nivel, index) =>
            <Text
              key={`${item.numero}-${nivel.numero}`}
              style={[styles.itemCode,]}
            > {nivel.nombre + ((index == 2 && item.niveles.length > 3) ? '...' : '')}</Text>
          )}

          {(!item.status) && (
            <Image style={styles.iconLock} tintColor="blue" source={require('../assets/lock.png')} />
          )}

          {item.status && item.status == 'Completado' && (
            <View style={{ position: 'absolute', right: 10, top: 5, flexDirection: 'row' }}>
              <Image style={styles.iconSuccess} tintColor="green" source={require('../assets/checked.png')} />
              <Text style={styles.itemCode}>nota: {item.nota}</Text>
            </View>
          )}

        </ImageBackground>
      </View>

    </TouchableOpacity>

  );

  return (
    <LinearGradient
      colors={['#1488CC', '#2B32B2']}
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <FlatGrid
        data={items.concat(leccions)}
        keyExtractor={item => `${item.numero}-${item.status}`}
        style={styles.gridView}
        spacing={10}
        renderItem={renderItem}
      />
      {/* Si no tiene nignguna leccion En proceso, es porque finalizo el curso */}
      <TouchableOpacity style={[styles.buttonCertificate, { opacity: currenLeccion || !currentUser.notaFinal ? 0.3 : 1 }]}
        disabled={currenLeccion || !currentUser.notaFinal}
        onPress={() => WebBrowser.openBrowserAsync(apiUrl + 'leccioninstance/certificado/' + currentUser._id)}>
        <Image style={styles.iconCertificate} tintColor="#D4AF37" source={require('../assets/certificate.png')} />
      </TouchableOpacity>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    flex: 1,
    justifyContent: "center",
    borderRadius: 15,
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    borderRadius: 15,
    top: 0
  },
  itemName: {
    fontSize: 16,
    fontWeight: '700',
    display: 'flex',
    justifyContent: 'center',
    alignSelf: 'flex-start',
    width: 'auto',
    paddingVertical: 4
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#000',
  },
  icon: {
    position: 'absolute',
    left: 40,
    top: 20,
    height: 70,
    width: 70,
  },
  iconSuccess: {
    marginHorizontal: 10,
    height: 25,
    width: 25,
  },
  iconLock: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    height: 30,
    width: 30,
    transform: [{ translateX: -5 }, { translateY: -5 }],
  },
  iconCertificate: {
    height: 50,
    width: 50,
  },
  buttonCertificate: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    backgroundColor: 'silver',
    borderRadius: 5,
    padding: 5,
  }
});
