import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { FlatGrid } from 'react-native-super-grid';
import { apiUrl, getLeccions, getLeccionsUser } from '../helpers/api';

export default function Usuario({ route, navigation }) {

  const [items, setItems] = useState([]);
  const [leccions, setLeccions] = useState([]);
  const [currenLeccion, setCurrenLeccion] = useState(1);
  const { userId } = route.params;

  useEffect(() => {
    getLeccionsUser(userId)
      .then(function (response) {
        console.log('lecionsInstances user',response.length);
        setItems(response);
        const current = response.findIndex(l => l.status === 'En proceso') + 1;
        setCurrenLeccion(current);
        getLeccions()
          .then(function (lecciones) {
            setLeccions(lecciones.filter(l => l.numero > current));
          })
          .catch(function (error) {
            console.log(error);
          })
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [])

  return (
    <LinearGradient
      colors={['#1488CC', '#2B32B2']}
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >

      <FlatGrid
        itemDimension={130}
        data={items.concat(leccions)}
        style={styles.gridView}
        keyExtractor={item => `${item.numero}`}
        spacing={10}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Respuestas', { leccionId: item._id, isOfUser: item.numero <= currenLeccion })}>
            <View
              style={[
                styles.itemContainer,
                {
                  backgroundColor: '#072b9d',
                  opacity: !item.status ? 0.3 : 1
                }
              ]}>
              <Image style={styles.icon} source={{ uri: apiUrl + 'images/lecciones/leccion-' + item.numero + '.png' }} />
              {item.status == 'Completado' && (
                <Image style={styles.iconSuccess} tintColor="green" source={require('../assets/checked.png')} />
              )}

              <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.itemName}>Lección {item.numero}</Text>
                {item.status == 'Completado' && (
                  <Text style={styles.itemCode}>nota: {item.nota}</Text>
                )}
              </View>

              <Text style={styles.itemCode}>{item.nombre}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 7,
    padding: 10,
    height: 150,
    top: 0
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  icon: {
    position: 'absolute',
    left: 40,
    top: 20,
    height: 70,
    width: 70,
  },
  iconSuccess: {
    position: 'absolute',
    right: 10,
    top: 10,
    height: 30,
    width: 30,
  },
  iconCertificate: {
    height: 50,
    width: 50,
  },
  buttonCertificate: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    backgroundColor: 'silver',
    borderRadius: 5,
    padding: 5,
  }
});
