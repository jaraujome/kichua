import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TextInput, Image, FlatList, TouchableOpacity, Modal, ActivityIndicator, Dimensions, Alert } from 'react-native';
import { signUp, getUsers } from '../helpers/api';
import BouncyCheckbox from "react-native-bouncy-checkbox";

const { width, height } = Dimensions.get('screen');

export default function Usuarios({ navigation }) {

    const [users, setUsers] = useState([]);
    const [search, onChangeSearch] = useState('');
    const [modalVisible, setModalVisible] = useState(false);
    const [username, setUsername] = React.useState('');
    const [name, setName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [isRoleAdmin, setRolAdmin] = React.useState(false);
    const [loading, setLoading] = React.useState(false);

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const handleSignUp = () => {
        if (username && email && password && name && lastName) {
            if (!validateEmail(email)) {
                Alert.alert('Error:', 'Email invalido');
                return;
            }
            setLoading(true)
            const infoUser = {
                username,
                email,
                password,
                name,
                lastName,
                isRoleAdmin
            }
            signUp(infoUser)
                .then(function (res) {

                    if (res && res.token) {
                        Alert.alert('Usuario Registrado:', 'Se ha registrado exitosamente el usuario')
                        setModalVisible(false)
                        setUsername('')
                        setName('')
                        setLastName('')
                        setEmail('')
                        setPassword('')
                        setRolAdmin(false)
                        getUsers()
                            .then(function (response) {
                                console.log('users: ', response.length);
                                setUsers(response);
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                    } else {
                        Alert.alert('Error: ', res.message ? res.message : '')
                    }
                    setLoading(false)
                })
                .catch(function (error) {
                    console.log(error);
                    Alert.alert('Error:', 'No se pudo crear el usuario');
                    setLoading(false)
                });
        } else {
            Alert.alert('Error:', 'Todos los campos son requeridos');
        }
    }

    useEffect(() => {

        getUsers()
            .then(function (response) {
                console.log('users: ', response.length);
                setUsers(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }, []);


    return (
        <>
            {loading && <View style={styles.spinner} pointerEvents={'none'}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>}

            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}>
                <Image style={{ width: 20, height: 20, }} source={require('../assets/search.png')} />
                <TextInput
                    inlineImageLeft='search_icon'
                    placeholder={'Buscar'}
                    style={{ padding: 10 }}
                    value={search}
                    onChangeText={onChangeSearch} />
            </View>

            {!users.length &&
                <Text style={{ padding: 20, textAlign: 'center' }}> No hay usuarios registrados </Text>
            }

            <FlatList
                data={users.filter(u => (((u.name + u.lastName + u.email).toLocaleLowerCase()).includes(search.toLocaleLowerCase())))}
                keyExtractor={item => `${item._id}`}
                renderItem={({ item }) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Usuario', { userId: item._id, title: item.username })}
                    >
                        <View style={{ backgroundColor: 'white', justifyContent: 'flex-end', paddingHorizontal: 10, paddingVertical: 5 }}>
                            <Text style={styles.itemName}> {item.email} </Text>
                            <Text style={styles.itemName}> {item.username} </Text>
                            <Text style={styles.notaEnd}>
                                {item.notaFinal ? 'Nota Certificado: ' + item.notaFinal : 'Lección ' + item.currentLeccion}
                            </Text>
                        </View>
                    </TouchableOpacity>
                )}
            />

            {/* Modal para registro de un nuevo usuario */}
            <Modal
                animationType="slide"
                visible={modalVisible}
                onRequestClose={() => { setModalVisible(!modalVisible); }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>

                        <Text style={{ borderColor: 'blue', color: 'white', fontSize: 20, fontWeight: 'bold' }}> Registrar Usuario </Text>

                        <TouchableOpacity
                            style={[styles.buttonClose]}
                            onPress={() => setModalVisible(!modalVisible)}
                        >
                            <Image style={{ width: 20, height: 20 }} tintColor="white" source={require('../assets/close.png')} />
                        </TouchableOpacity>

                        <View style={[styles.inputForm, { borderColor: 'blue', alignItems: 'flex-start', paddingHorizontal: 0 }]}>

                            <BouncyCheckbox
                                size={20}
                                fillColor="green"
                                unfillColor="#FFFFFF"
                                text='Admin'
                                disableBuiltInState
                                textStyle={{ textDecorationLine: 'none', color: 'white' }}
                                iconStyle={{ borderColor: "blue" }}
                                isChecked={isRoleAdmin}
                                onPress={() => setRolAdmin(!isRoleAdmin)}
                            />
                        </View>

                        <View style={styles.inputForm}>
                            <TextInput
                                placeholder='Usuario'
                                placeholderTextColor='white'
                                style={{ paddingHorizontal: 10 }}
                                value={username}
                                onChangeText={(username) => setUsername(username)}
                            />
                        </View>

                        <View
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                flexDirection: 'row'
                            }}>
                            <TextInput
                                placeholder='Nombre'
                                placeholderTextColor='white'
                                value={name}
                                style={[styles.inputForm, { maxWidth: width / 2 - 65, paddingHorizontal: 10, margin: 0 }]}
                                onChangeText={(name) => setName(name)}
                            />
                            <TextInput
                                placeholder='Apellido '
                                placeholderTextColor='white'
                                value={lastName}
                                style={[styles.inputForm, { maxWidth: width / 2 - 65, paddingHorizontal: 10, margin: 0 }]}
                                onChangeText={(lastName) => setLastName(lastName)}
                            />
                        </View>

                        <View
                            style={styles.inputForm}>
                            <TextInput
                                placeholder='Correo Electronico'
                                placeholderTextColor='white'
                                value={email}
                                onChangeText={(email) => setEmail(email)}
                            />
                        </View>

                        <View style={styles.inputForm}>

                            <TextInput
                                secureTextEntry={true}
                                placeholder='Contraseña'
                                placeholderTextColor='white'
                                value={password}
                                style={{ paddingHorizontal: 10 }}
                                onChangeText={(password) => setPassword(password)}
                            />
                        </View>

                        <TouchableOpacity
                            style={[styles.button, { backgroundColor: '#c69723', marginTop: 20 }]}
                            onPress={() => handleSignUp()}>
                            <Text style={{ color: 'white' }}> Registrar</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>

            {/* Boton para agregar nuevo usuario */}
            <TouchableOpacity
                style={[styles.buttonOpen]}
                onPress={() => setModalVisible(true)}
            >
                <Image style={{ width: 50, height: 50 }} tintColor="blue" source={require('../assets/add.png')} />
            </TouchableOpacity>
        </>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        marginHorizontal: 10,
        backgroundColor: "blue",
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        right: 20,
        bottom: 20,
        position: 'absolute',
    },
    buttonClose: {
        position: 'absolute',
        top: 15,
        right: 15,
        width: 'auto'
    },
    itemName: {
        fontSize: 16,
        color: 'black',
        fontWeight: '600',
    },
    notaEnd: {
        fontSize: 16,
        color: 'black',
        fontWeight: '600',
        alignSelf: 'flex-end',
    },
    inputForm: {
        alignItems: 'center',
        textAlign: 'center',
        marginHorizontal: 20,
        marginVertical: 10,
        borderWidth: 2,
        paddingHorizontal: 20,
        borderColor: "#FFFEFE",
        borderRadius: 23,
        paddingVertical: 2,
        width: width - 100,
    },
    spinner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'black',
        width,
        height: height - 30,
        opacity: 0.6,
        zIndex: 1,
    },
});
