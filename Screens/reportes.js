import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, ActivityIndicator, Dimensions, Alert } from 'react-native';
import { getLeccionInstances } from '../helpers/api';
import { PieChart } from "react-native-chart-kit";
import BouncyCheckbox from "react-native-bouncy-checkbox";
const { width, height } = Dimensions.get('screen');

export default function Usuarios({ navigation }) {

    const [leccionsInstances, setLeccionsInstances] = useState([]);
    const [leccions, setLeccions] = useState([]);
    const [loading, setLoading] = React.useState(true);
    const [reports, setReports] = React.useState({
        aprobadas: 0,
        enProceso: 0,
        preguntasMasFallos: [],
        users: [],
    });

    useEffect(() => {
        const willFocusSubscription = navigation.addListener('focus', () => {
            getReports();
        });
        return willFocusSubscription;
    }, []);

    function isValid(pregunta) {
        const removeAccents = (str) => {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }
        return removeAccents(pregunta.respuesta.toLowerCase().trim()) === removeAccents(pregunta.respuestaCorrecta.toLowerCase().trim());
    }

    const getReports = () => {
        let aprobadas = 0, enProceso = 0;
        let preguntasFallidas = {};
        let preguntasMasFallos = [];
        let users = [];
        let preguntasValidas = {};
        const sK = '-:XX:-'; // separador

        getLeccionInstances()
            .then(function (response) {
                console.log('leccionsInstances: ', response.length);
                setLeccionsInstances(response);

                // iterar lecciones de los usuarios para obtener reportes
                response.forEach(l => {
                    if (l.status == 'Completado') {
                        aprobadas++;
                    } else {
                        enProceso++;
                    }
                    if (!users.includes(l.user)) {
                        users.push(l.user);
                    }
                    // contar el numero de fallos (preguntas con respuestas invalidas)
                    l.evaluacion.forEach(p => {
                        if (p.respuesta) {
                            const keyP = p.texto + sK + p.tipo + sK + p.respuestaCorrecta + sK + p.numero + sK + l.numero;
                            if (!preguntasFallidas[keyP]) {
                                preguntasFallidas[keyP] = 0;
                                preguntasValidas[keyP] = 0;
                            }
                            if (isValid(p)) {
                                preguntasValidas[keyP]++;
                            } else {
                                preguntasFallidas[keyP]++;
                            }
                        }
                    });
                });

                Object.entries(preguntasFallidas).forEach(([key, value]) => {
                    const decodeKey = key.split(sK);
                    if (decodeKey.length > 4) {
                        const pregunta = {
                            total: value + preguntasValidas[key], // total = fallidas + validas
                            texto: decodeKey[0],
                            tipo: decodeKey[1],
                            respuestaCorrecta: decodeKey[2],
                            leccionNumero: decodeKey[3],
                            numero: decodeKey[4],
                            textoSplit: decodeKey[0].split(decodeKey[2]),
                        }
                        preguntasMasFallos.push({ ...pregunta, cantidad: value, });
                    }
                });

                // top 5 preguntas con mayor cantidad de fallos
                preguntasMasFallos = preguntasMasFallos.filter(p => p.cantidad)
                    .sort((a, b) => b.cantidad - a.cantidad).slice(0, 5);

                setReports({ aprobadas, enProceso, preguntasMasFallos, users })
                setLoading(false);
            })
            .catch(function (error) {
                console.log(error);
                setLoading(false);
            });
    }

    return (
        <ScrollView style={{ padding: 15, marginBottom: 15 }}>
            {loading && <View style={styles.spinner} pointerEvents={'none'}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>}

            <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
                <Image style={{ width: 20, height: 20, marginHorizontal: 10 }} tintColor="blue"
                    source={require('../assets/drawer/baseline_groups_black_18.png')} />
                <Text>Usuarios: {reports.users.length}</Text>
            </View>
            <Text> Total lecciones: {leccionsInstances.length} </Text>
            <Text>  </Text>
            <PieChart
                data={[
                    {
                        name: " Aprobadas",
                        cantidad: reports.aprobadas,
                        color: "#0F0",
                        legendFontColor: "#000",
                        legendFontSize: 14
                    },
                    {
                        name: " En proceso",
                        cantidad: reports.enProceso,
                        color: "#00F",
                        legendFontColor: "#000",
                        legendFontSize: 14
                    },
                ]}
                width={width}
                height={100}
                chartConfig={{
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                }}
                accessor={"cantidad"}
                backgroundColor={"transparent"}
                center={[0, 0]}
                absolute
            />
            <Text>  </Text>
            <Text> Preguntas con mas fallos: </Text>
            {reports.preguntasMasFallos.slice(0, 10).map((item, index) =>
                <View style={{ padding: 15 }} key={index}>

                    {item.tipo !== 'Completación' && (
                        <>
                            <Text>{item.texto} </Text>
                            <View style={{ justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center' }}>
                                <BouncyCheckbox
                                    size={20}
                                    fillColor="blue"
                                    unfillColor="#FFFFFF"
                                    text='V'
                                    disabled
                                    disableBuiltInState
                                    textStyle={{ textDecorationLine: 'none' }}
                                    iconStyle={{ borderColor: "blue" }}
                                    isChecked={(item.respuestaCorrecta === 'true')}
                                />
                                <BouncyCheckbox
                                    size={20}
                                    fillColor="red"
                                    unfillColor="#FFFFFF"
                                    text='F'
                                    disabled
                                    disableBuiltInState
                                    textStyle={{ textDecorationLine: 'none' }}
                                    iconStyle={{ borderColor: "red" }}
                                    isChecked={(item.respuestaCorrecta === 'false')}
                                />
                            </View>
                        </>
                    )}

                    {item.tipo === 'Completación' && (
                        <Text>
                            <Text>{item.textoSplit[0]}</Text>
                            <Text style={{ textDecorationLine: 'underline' }}>{item.respuestaCorrecta} </Text>
                            {item.textoSplit[1] &&
                                <Text>{item.textoSplit[1]}</Text>
                            }
                        </Text>
                    )}
                    <Text>cantidad: {item.cantidad} / {item.total}  Lección: {item.leccionNumero}</Text>
                </View>
            )}
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    spinner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'black',
        width,
        height: height - 30,
        opacity: 0.6,
        zIndex: 1,
    },
});
