import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Home from './home';
import { Title, Caption, Chip, Drawer } from 'react-native-paper';
import { apiUrl, authService, signOut, getLeccionsUser, getLeccions } from '../helpers/api';

const { width, height } = Dimensions.get('screen');

const colores = [
  '#072b9d', '#ffdd77', '#de7878', '#51c2d4', '#6e6fd4',
  '#e8d54a', '#ff6961', '#77dd77', '#fd961c', '#ff6565',
  '#75f9f2', '#ffdd77', '#de7878', '#b186f1', '#e8d54a',
];

function Menu({ navigation }) {

  const [currentUser, setCurrentUser] = useState(false)
  const [isAdmin, setIsAdmin] = useState(false)
  const [leccions, setLeccions] = useState([]);
  const [leccionsUser, setLeccionsUser] = useState([]);
  const [currentLeccion, setCurrentLeccion] = useState({ numero: 1 });

  useEffect(() => {

    const willFocusSubscription = navigation.addListener('focus', () => {
      const x = authService.currentUserValue;
      setCurrentUser(x)
      setIsAdmin(x && x.roles && (x.roles.indexOf('admin') !== -1))
      if (!isAdmin) {
        getLeccionsUser(x._id)
          .then(function (response) {
            setLeccionsUser(response);
            const current = response.find(l => l.status === 'En proceso');
            setCurrentLeccion(current);
            getLeccions()
              .then(function (lecciones) {
                setLeccions(lecciones.filter(l => l.numero > current));
              })
              .catch(function (error) {
                console.log(error);
              })
          })
          .catch(function (error) {
            console.log(error);
          })
      }
    });
    return willFocusSubscription;
  }, [])

  function logout() {
    signOut();
    navigation.navigate('Login');
  }

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView>
        <View style={styles.drawerContent}>
          <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
              <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: width / 1.5 }}>
                <Title style={styles.Title}>Menu  </Title>
                <Chip style={styles.chipInfo} selectedColor='white'> {isAdmin ? 'Admin' : 'Usuario'} </Chip>
              </View>
              <Caption>{currentUser.email}</Caption>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
          {!isAdmin && (
            <DrawerItem
              key={'lecciones'}
              style={{ backgroundColor: '#006df6' }}
              icon={() => (
                <Image tintColor='white' source={require('../assets/drawer/baseline_school_black_18.png')} />
              )}
              inactiveTintColor='white'
              label="Lecciones"
              onPress={() => navigation.navigate('Lecciones')}
            />
          )}

          {!isAdmin &&
            <>
              {leccionsUser.length == 1 &&
                <Text style={{ padding: 10 }}>Ir a Lección:</Text>
              }
              {leccionsUser.length > 1 &&
                <Text style={{ padding: 15 }}>Continuar con:</Text>
              }
              <DrawerItem
                style={[styles.linkLeccion, { borderColor: colores[currentLeccion.numero] }]}
                key={'currentLeccion'}
                icon={() => (
                  <Image style={{ width: 27, height: 27 }} source={{ uri: apiUrl + 'images/lecciones/leccion-' + currentLeccion.numero + '.png' }} />
                )}
                inactiveTintColor='white'
                label={'' + currentLeccion.nombre}
                onPress={() => navigation.navigate('Leccion', { leccionId: currentLeccion._id, numero: currentLeccion.numero, total: 100 })}
              />
              {leccionsUser.length > 1 &&
                <Text style={{ padding: 15 }}>Lecciones aprobadas:</Text>
              }
            </>
          }

          {!isAdmin && leccionsUser.filter(l => l.numero != currentLeccion.numero).map((leccion) =>
            <DrawerItem
              style={[styles.linkLeccion, { borderColor: colores[leccion.numero] }]}
              key={leccion.numero}
              icon={() => (
                <Image style={{ width: 27, height: 27 }} source={{ uri: apiUrl + 'images/lecciones/leccion-' + leccion.numero + '.png' }} />
              )}
              inactiveTintColor='white'
              label={'' + leccion.nombre}
              onPress={() => navigation.navigate('Leccion', { leccionId: leccion._id, numero: leccion.numero, total: 100 })}
            />
          )}

          {isAdmin && (
            <DrawerItem
              key={'config'}
              icon={() => (
                <Image source={require('../assets/drawer/baseline_settings_black_18.png')} />
              )}
              label="Configuración"
              onPress={() => navigation.navigate('Configuracion')}
            />
          )}
          {isAdmin && (
            <DrawerItem
              key={'users'}
              icon={() => (
                <Image source={require('../assets/drawer/baseline_groups_black_18.png')} />
              )}
              label="Usuarios"
              onPress={() => navigation.navigate('Usuarios')}
            />
          )}
          {isAdmin && (
            <DrawerItem
              key={'reportes'}
              icon={() => (
                <Image style={{ width: 27, height: 27 }} source={require('../assets/stats.png')} />
              )}
              label="Reportes"
              onPress={() => navigation.navigate('Reportes')}
            />
          )}
        </Drawer.Section>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={() => (
            <Image source={require('../assets/drawer/baseline_login_black_18.png')} />
          )}
          label="Salir"
          onPress={() => logout()}
        />
      </Drawer.Section>
    </View>
  );
}

const DrawerNav = createDrawerNavigator();

export default function Drawer1() {
  return (

    <DrawerNav.Navigator
      drawerContent={(props) => <Menu {...props} />}
      drawerStyle={{
        backgroundColor: 'white',
      }}
      drawerContentOptions={{
        activeTintColor: 'blue',
        inactiveTintColor: 'white'
      }}
    >
      <DrawerNav.Screen exact path='/' name="Home" component={Home} />

    </DrawerNav.Navigator>

  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#33b5ff',
    borderTopWidth: 1
  },
  chipInfo: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: '#006df6',
    alignSelf: 'flex-end',
  },
  linkLeccion: {
    backgroundColor: '#006df6',
    borderRightWidth: 10,
    borderLeftWidth: 10,
  }
});

