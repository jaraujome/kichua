
import React, {useEffect} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { createStackNavigator, StackScreenProps } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerItemList} from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { Header, Left } from 'native-base';

export default function Home( { navigation } ) {
  
  
  return (
   <>
    <Header style={{backgroundColor:'#4e95c2'}}>
      <Left>
        <TouchableOpacity style={styles.icon} onPress={() => navigation.openDrawer()}>
         <Image  source={require('../assets/iconoh.png') }  />
        </TouchableOpacity>
      </Left>
    </Header>

    <LinearGradient
        colors={['#1488CC', '#2B32B2']}
        style={styles.container}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
      >
       <Text style={styles.text}>Proyecto Kichua Multiplataforma</Text>
       <Text style={styles.text1}>Centro de Formacion Academica de Ecuador</Text>
        <View style={styles.svg}>
         <Image source={require('../assets/front.png')}/>
        </View>
        <View > 
           <Text style={styles.footer}>Enseñanza del Idioma Kichua a Varios Niveles</Text>
           <Text style={styles.subfooter}>República del Ecuador</Text>
        </View>
          
        
      </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  svg:{
    position: 'absolute',
    width: 304,
    height: 258,
    left: 24,
    top: 102,
  },
  footer:{
    textAlign: 'center',
    color: '#FFFEFE',
    top: 120,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 16,
  },
  subfooter:{
    textAlign: 'center',
    color: '#FFFEFE',
    top: 130,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 14,
    lineHeight: 16,
  },
  text:{
    position: 'absolute',
    width: 203,
    height: 20,
    left: 75,
    top: 20,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 13,
    lineHeight: 15,
    textAlign: 'center',
    color: '#FFFEFE',
  },
  text1:{
    position: 'absolute',
    width: 255,
    height: 14,
    left: 57,
    top: 40,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    lineHeight: 14,
    textAlign: 'center',
    color: '#FFFEFE',
  },

  icon: {
    position: 'absolute',
    left: -90,
    top: -10,
    
 },
});

