import { BehaviorSubject } from 'rxjs';
import 'localstorage-polyfill';

export const apiUrl = 'http://192.168.1.105:4000/';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));

export const authService = {
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
};

export const signUp = async (infoUser) => {

    const { username, email, password, name, lastName, roleAdmin } = infoUser;

    const res = await fetch(apiUrl + 'signup', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            username,
            email: email.toLocaleLowerCase(),
            password,
            name,
            lastName,
            roles: roleAdmin ? ['admin'] : null
        })
    });
    const user = await res.json();
    console.log('userRegitered', user)
    return user;
}

export const signIn = async (email, password) => {

    const res = await fetch(apiUrl + 'signin', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email: email.toLocaleLowerCase(), password })
    });
    const user = await res.json();
    localStorage.setItem('currentUser', JSON.stringify(user));
    currentUserSubject.next(user);
    console.log(user)
    return user;
}

export const signOut = () => {
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
}

// obtener todas las lecciones
export const getLeccions = async () => {
    const res = await fetch(apiUrl + 'leccion');
    const leccions = await res.json();
    console.log('Lecciones Obtenidas::', leccions.length)
    return leccions;
}

// obtener una leccion por su id
export const getLeccion = async (leccionId) => {
    const res = await fetch(apiUrl + 'leccion/' + leccionId);
    const leccion = await res.json();
    console.log('Leccion:', leccion.nombre)
    return leccion;
}

// obtener los usuarios
export const getUsers = async () => {
    const res = await fetch(apiUrl + 'admin/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'x-access-token': authService.currentUserValue.token
        },
        body: JSON.stringify({'userId': authService.currentUserValue._id })
    });

    const users = res.status == '200' ? await res.json() : [];
    return users;
}

export const addLeccion = async (leccion) => {
    const res = await fetch(apiUrl + 'leccion/create', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(leccion)
    });
    const newLeccion = await res.json();
    console.log('Leccion creada')
    return newLeccion;
}

export const removeLeccion = async (leccionId) => {

    const res = await fetch(apiUrl + 'leccion/' + leccionId, { method: 'DELETE' });
    const leccion = await res.json();
    console.log('Leccion eliminada')
    return leccion;
}

export const updateLeccion = async (leccion) => {

    const res = await fetch(apiUrl + 'leccion/' + leccion._id, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(leccion)
    });
    const data = await res.json();

    return res;
}

// obtiene todas las leccionesInstances de un usuario
export const getLeccionsUser = async (userId) => {
    const res = await fetch(apiUrl + 'leccionInstance/user/' + userId);
    const leccions = await res.json();
    console.log('Leccions User:', leccions.length)

    return leccions;
}

// obtiene todas las leccionesInstances
export const getLeccionInstances = async () => {
    const res = await fetch(apiUrl + 'leccionInstance');
    const leccionInstances = await res.json();
    return leccionInstances;
}

// obtiene una leccionesInstance por su id
export const getLeccionInstance = async (leccionInstanceId) => {
    const res = await fetch(apiUrl + 'leccionInstance/' + leccionInstanceId);
    const leccionInstance = await res.json();
    console.log('Leccion Instance:', leccionInstance.nombre)
    return leccionInstance;
}

export const updateLeccionInstance = async (leccionInstance) => {

    const res = await fetch(apiUrl + 'leccionInstance/' + leccionInstance._id, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(leccionInstance)
    });
    const data = await res.json();

    return res;
}

// cargar recurso pdf
export const uploadFile = async (file) => {
    const formData = new FormData();
    formData.append('recurso', file);
    formData.append('name', 'Recurso.pdf');
    formData.append('tipo', 'pdf');
    const res = await fetch(apiUrl + 'upload/recurso', {
        method: 'POST',
        body: formData,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
    });
    const data = await res.json();

    return data;
}
