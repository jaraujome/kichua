import React from 'react';
import { AppRegistry } from 'react-native';
import Drawer from './Screens/drawer';
import Login from './Screens/login';
import SignUp from './Screens/signup';
import Lecciones from './Screens/lecciones';
import Configuracion from './Screens/configuracion';
import Usuarios from './Screens/usuarios';
import Usuario from './Screens/usuario';
import Leccion from './Lecciones/Leccion';
import Respuestas from './LeccionesAdmin/Respuestas';
import Reportes from './Screens/reportes';
import LeccionAdmin from './LeccionesAdmin/LeccionAdmin';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { name as appName } from './app.json'

const Stack = createStackNavigator();

function App({ navigation }) {
  return (

    <NavigationContainer>

      <Stack.Navigator>

        <Stack.Screen
          exact path='/'
          name="Login"
          component={Login}
          options={{
            headerStyle: {
              backgroundColor: '#4e95c2'
            },
            headerTintColor: 'white',
            title: 'Bienvenidos al Conocimiento',
            headerTitleStyle: {
              fontWeight: 'bold',
              alignSelf: 'center'
            },

          }}
        />

        <Stack.Screen
          name="Inicio"
          component={Drawer}
          options={{
            headerStyle: {
              backgroundColor: '#4e95c2'
            },
            headerTintColor: 'white',
            title: 'Menu Principal de la App',
            headerTitleStyle: {
              fontWeight: 'bold',
              alignSelf: 'center'
            },
            headerShown: false
          }}
        />

        <Stack.Screen name="Lecciones" component={Lecciones} options={{ headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

        <Stack.Screen name="Configuracion" component={Configuracion} options={{ title: 'Configuración', headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

        <Stack.Screen name="Usuarios" component={Usuarios} options={{ headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

        <Stack.Screen name="Reportes" component={Reportes} options={{ headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

        <Stack.Screen
          name="Usuario"
          component={Usuario}
          options={({ route }) => ({
            title: route.params.title,
            headerStyle: { backgroundColor: '#4e95c2' },
            headerTintColor: 'white'
          })}
        />

        <Stack.Screen name="Respuestas" component={Respuestas} options={{ headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

        <Stack.Screen
          name="Leccion"
          component={Leccion}
          options={(
            { route }) => ({
              title: 'Lección ' + route.params.numero,
              headerStyle: {
                backgroundColor: '#4e95c2'
              },
              headerTintColor: 'white',
            })}
        />

        <Stack.Screen
          name="LeccionAdmin"
          component={LeccionAdmin}
          options={(
            { route }) => ({
              title: 'Editar Lección ' + route.params.numero,
              headerStyle: {
                backgroundColor: '#4e95c2'
              },
              headerTintColor: 'white',
            })}
        />

        <Stack.Screen name="SignUp" component={SignUp} options={{ headerStyle: { backgroundColor: '#4e95c2' }, headerTintColor: 'white', }} />

      </Stack.Navigator>

    </NavigationContainer>

  );
}

export default App;

AppRegistry.registerComponent(appName, () => App);