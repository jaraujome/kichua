import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, TouchableOpacity, Switch, Image, View, ActivityIndicator, FlatList, StyleSheet, ToastAndroid, Dimensions, Alert } from 'react-native';
import { updateLeccion } from '../helpers/api';
import BouncyCheckbox from "react-native-bouncy-checkbox";

const { width, height } = Dimensions.get('screen');

export default function Evaluacion(props) {

    const [leccion, setLeccion] = useState(props.leccion)
    const [loading, setLoading] = useState(false)

    async function updateChanges() {
        setLoading(true)
        updateLeccion(leccion)
            .then(function (response) {
                ToastAndroid.show('Cambios Guardados!', ToastAndroid.SHORT);
                setLoading(false);
            })
            .catch(function (error) {
                console.log(error);
                ToastAndroid.show('Error al Guardar!', ToastAndroid.SHORT);
                setLoading(false);
            });
    }

    // para cambiar tipo de pregunta
    function toggleSwitch(value, pregunta) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.evaluacion[pregunta.numero - 1].tipo = value ? 'Completación' : 'Cuestionario';
        const respuestaCorrecta = newLeccion.evaluacion[pregunta.numero - 1].respuestaCorrecta
        if (value) {
            newLeccion.evaluacion[pregunta.numero - 1].respuestaCorrecta = '';
        }
        setLeccion(newLeccion);
    }

    // para cambiar respuesta en tipo cuestionario (V o F)
    async function toggleRepuesta(value, pregunta) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.evaluacion[pregunta.numero - 1].respuestaCorrecta = value ? 'true' : 'false';
        console.log(newLeccion.evaluacion[pregunta.numero - 1].respuestaCorrecta)
        console.log(pregunta.numero - 1, 'RRR')
        setLeccion(newLeccion);
        await updateChanges();
        return value
    }

    // comparar si es valida la respuestaCorrecta 
    function isValid(pregunta) {
        const removeAccents = (str) => {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }
        const allText = removeAccents(pregunta.texto.toLowerCase().trim());
        const fraseOculta = removeAccents(pregunta.respuestaCorrecta.toLowerCase().trim());
        return allText.indexOf(fraseOculta) !== -1;
    }

    // cambiar un campos de la pregunta
    function changePregunta(value, key, pregunta) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.evaluacion[pregunta.numero - 1][key] = value;
        if (key == 'respuestaCorrecta') {
            if (!isValid(newLeccion.evaluacion[pregunta.numero - 1])) {
                return;
            }
        }
        setLeccion(newLeccion);
    }

    function agregarPregunta() {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.evaluacion.push({
            numero: newLeccion.evaluacion.length + 1,
            texto: '',
            tipo: 'Cuestionario',
            respuestaCorrecta: '',
            salto: 0,
            nota: 0,
        });
        setLeccion(newLeccion);
    }

    async function removePregunta(index) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.evaluacion.splice(index, 1);
        newLeccion.evaluacion.map((pregunta, i) => ({ ...pregunta, numero: (i + 1) }));
        setLeccion(newLeccion);
        await updateChanges();
    }

    return (
        <SafeAreaView style={styles.container}>

            {loading && <View style={styles.spinner} pointerEvents={'none'}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>}

            <FlatList
                data={leccion.evaluacion}
                keyExtractor={item => `${item.numero}`}
                contentContainerStyle={{
                    paddingBottom: !leccion.evaluacion.filter(p => p.tipo == 'Cuestionario').length ? 200 : 100
                }}
                renderItem={({ item, index }) => {
                    return (
                        <View style={{ borderBottomWidth: (index + 1) == leccion.evaluacion.length ? 0 : 1 }}>
                            <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>

                                <Text style={{ paddingVertical: 15 }} > Pregunta {item.numero}</Text>

                                <TouchableOpacity onPress={() => removePregunta((item.numero - 1))}>
                                    <Image
                                        style={{ width: 18, height: 18 }}
                                        source={require('../assets/trash.png')}
                                    />
                                </TouchableOpacity>
                            </View>

                            <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                                <Text> Pregunta de Completación </Text>
                                <Switch
                                    trackColor={{ false: "#767577", true: "#81b0ff" }}
                                    thumbColor={item.tipo == 'Completación' ? "blue" : "#f4f3f4"}
                                    ios_backgroundColor="#3e3e3e"
                                    activeText={'Completación'}
                                    inActiveText={'Cuestionario'}
                                    onValueChange={(value) => toggleSwitch(value, item)}
                                    value={(item.tipo == 'Completación')}
                                />
                            </View>

                            <TextInput
                                placeholder="Escriba la Pregunta"
                                value={item.texto}
                                style={{ padding: 10 }}
                                onChangeText={text =>
                                    changePregunta(text, 'texto', item)
                                }
                                onEndEditing={async (e) =>
                                    await updateChanges()
                                }
                            />

                            {item.tipo === 'Completación' && (
                                <TextInput
                                    placeholder="Respuesta Correcta, (ingrese texto a ocultar)"
                                    value={item.respuestaCorrecta}
                                    style={{ paddingHorizontal: 10 }}
                                    onChangeText={text =>
                                        changePregunta(text, 'respuestaCorrecta', item)
                                    }
                                    onEndEditing={async (e) =>
                                        await updateChanges()
                                    }
                                />
                            )}

                            {item.tipo !== 'Completación' && (
                                <View style={{ justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{ paddingRight: 35 }} > Respuesta Correcta</Text>

                                    <BouncyCheckbox
                                        size={20}
                                        fillColor="blue"
                                        unfillColor="#FFFFFF"
                                        text='V'
                                        disableBuiltInState
                                        textStyle={{ textDecorationLine: 'none' }}
                                        iconStyle={{ borderColor: "blue" }}
                                        isChecked={(item.respuestaCorrecta === 'true')}
                                        onPress={(value) => toggleRepuesta(!value, item)}
                                    />

                                    <BouncyCheckbox
                                        size={20}
                                        fillColor="red"
                                        unfillColor="#FFFFFF"
                                        text='F'
                                        disableBuiltInState
                                        textStyle={{ textDecorationLine: 'none' }}
                                        iconStyle={{ borderColor: "red" }}
                                        isChecked={(item.respuestaCorrecta === 'false')}
                                        onPress={(value) => toggleRepuesta(value, item)}
                                    />
                                </View>
                            )}

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={{ padding: 15 }} > Nota: </Text>
                                <TextInput
                                    keyboardType="numeric"
                                    placeholder="Nota"
                                    value={item.nota.toString()}
                                    style={{ padding: 10 }}
                                    onChangeText={text =>
                                        changePregunta(text, 'nota', item)
                                    }
                                    onEndEditing={async (e) =>
                                        await updateChanges()
                                    }
                                />
                            </View>

                        </View>
                    );
                }}
            />

            <TouchableOpacity
                onPress={() => agregarPregunta()}>
                <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ paddingRight: 35 }} > Agregar Pregunta </Text>
                    <Image
                        style={{ width: 18, height: 18 }}
                        source={require('../assets/add.png')}
                    />
                </View>
            </TouchableOpacity>
        </SafeAreaView>

    );
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        paddingBottom: 30,
    },
    space: {
        padding: 10,
        textAlign: 'center',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center'
    },
    loading: {
        backgroundColor: 'black',
        opacity: 0.3,
        position: 'absolute',
        width,
        height: height - 30,
        zIndex: 1,
    },
    spinner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'black',
        width,
        height: height - 30,
        opacity: 0.6,
        zIndex: 1,
    },
});
