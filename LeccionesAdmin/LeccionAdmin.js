import React, { useState, useEffect } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { getLeccion } from '../helpers/api';
import { Header, Left } from 'native-base';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Contenido from './Contenido';
import Evaluacion from './Evaluacion';
import Actividad from './Actividad';
import Recursos from './Recursos';

export default function LeccionAdmin({ route, navigation }) {

  const Tab = createBottomTabNavigator();

  const { leccionId } = route.params;

  const [leccion, setLeccion] = useState({ niveles: [], evaluacion: [] })

  useEffect(() => {
    getLeccion(leccionId)
      .then(function (response) {
        const data = response;
        console.log(data);
        setLeccion(data);
      })
      .catch(function (error) {
        //console.log(error);
      });
  }, [])

  return (
    <Tab.Navigator
      initialRouteName={'Actividad'}
      tabBarOptions={{
        activeTintColor: 'blue',
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconSrc = require('../assets/file.png');

          if (route.name === 'Contenido') {
            iconSrc = require('../assets/open-book.png');
          } else if (route.name === 'Recursos') {
            iconSrc = require('../assets/folder.png');
          } else if (route.name === 'Evaluacion') {
            iconSrc = require('../assets/checklist.png');
          }
          return <Image source={iconSrc} fadeDuration={0} style={{ width: 30, height: 30, opacity: focused ? 1 : 0.5 }} />
        },
      })}
      tabBarOptions={{
        keyboardHidesTabBar: true,
      }}
    >
      <Tab.Screen name="Actividad" children={() => <Actividad leccion={leccion} leccionId={leccionId} />} options={{ unmountOnBlur: true, }} />
      <Tab.Screen name="Contenido" children={() => <Contenido leccion={leccion} />} />
      <Tab.Screen name="Recursos" children={() => <Recursos leccion={leccion} />} />
      <Tab.Screen name="Evaluacion" children={() => <Evaluacion leccion={leccion} />} />
    </Tab.Navigator>
  );
}
