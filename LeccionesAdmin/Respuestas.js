import React, { useState, useEffect } from 'react';
import { Animated, Text, TextInput, TouchableOpacity, Image, View, Alert, FlatList, StyleSheet, Dimensions } from 'react-native';
import { getLeccion, getLeccionInstance } from '../helpers/api';
import BouncyCheckbox from "react-native-bouncy-checkbox";

const { width, height } = Dimensions.get('screen');

export default function Respuestas({ route }) {

    const { leccionId, isOfUser } = route.params;
    const [leccion, setLeccion] = useState({ evaluacion: [] })

    useEffect(() => {
        if (isOfUser) {
            getLeccionInstance(leccionId)
                .then(function (response) {
                    setLeccion(response);
                })
                .catch(function (error) {
                    //console.log(error);
                });
        } else {
            // el usuario no tiene asignada la leccion todavia
            getLeccion(leccionId)
                .then(function (response) {
                    setLeccion(response);
                })
                .catch(function (error) {
                    //console.log(error);
                });
        }
    }, [])


    function isValid(pregunta) {
        const removeAccents = (str) => {
            return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        }
        return removeAccents(pregunta.respuesta.toLowerCase().trim()) === removeAccents(pregunta.respuestaCorrecta.toLowerCase().trim());
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <FlatList
                data={leccion.evaluacion}
                keyExtractor={item => `${item.numero}`}
                style={{ flexGrow: 1 }}
                renderItem={({ item, index }) => {
                    return (
                        <>
                            {isOfUser && !!item.respuesta && isValid(item) && (
                                <Image style={{ position: 'absolute', top: 15, left: 5, height: 16, width: 16, }} tintColor="green" source={require('../assets/checked.png')} />
                            )}

                            {isOfUser && !!item.respuesta && !isValid(item) && (
                                <Image style={{ position: 'absolute', top: 15, left: 5, height: 16, width: 16, }} tintColor="red" source={require('../assets/close.png')} />
                            )}

                            {item.tipo === 'Completación' && (
                                <>
                                    <Text style={{ padding: 15, paddingLeft: 35 }}>
                                        {
                                            item.texto.replace(item.respuestaCorrecta, '___________')
                                        }
                                    </Text>
                                    <TextInput
                                        placeholder="Respuesta"
                                        editable={false}
                                        value={item.respuesta}
                                        style={{ paddingHorizontal: 15 }}
                                    />
                                </>
                            )}

                            {item.tipo !== 'Completación' && (
                                <>
                                    <Text style={{ padding: 15, paddingLeft: 35 }} > {item.texto}</Text>
                                    <View style={{ paddingBottom: 15, justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center' }}>
                                        <BouncyCheckbox
                                            size={20}
                                            fillColor="blue"
                                            unfillColor="#FFFFFF"
                                            text='V'
                                            disabled={true}
                                            disableBuiltInState
                                            textStyle={{ textDecorationLine: 'none' }}
                                            iconStyle={{ borderColor: "blue" }}
                                            isChecked={(item.respuesta === 'true')}
                                        />
                                        <BouncyCheckbox
                                            size={20}
                                            fillColor="red"
                                            unfillColor="#FFFFFF"
                                            text='F'
                                            disabled={true}
                                            disableBuiltInState
                                            textStyle={{ textDecorationLine: 'none' }}
                                            iconStyle={{ borderColor: "red" }}
                                            isChecked={(item.respuesta === 'false')}
                                        />
                                    </View>
                                </>

                            )}

                        </>
                    );
                }}
            />

            {leccion.status == 'Completado' && (
                <View style={{ paddingHorizontal: 10, justifyContent: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ paddingVertical: 15 }} > Nota: {leccion.nota} </Text>
                </View>
            )}
        </View>
    );
}
