import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, StyleSheet, StatusBar, ToastAndroid, ActivityIndicator, Image, TouchableOpacity, Dimensions } from 'react-native';
import QuillEditor, { QuillToolbar } from 'react-native-cn-quill';
import { getLeccion, updateLeccion } from '../helpers/api';

const { width, height } = Dimensions.get('screen');

export default function Actividad(props) {

    const [loading, setLoading] = useState(false)
    const [leccion, setLeccion] = useState(props.leccion)
    const [edited, setEdited] = useState(false)
    const [leccionId, setLeccionId] = useState(props.leccionId)

    const _editor = React.createRef();

    function handleChange(data) {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.actividad = data;
        setLeccion(newLeccion)
        if (!edited) {
            setEdited(true);
        }
    }

    function updateChanges() {
        setLoading(true)
        updateLeccion(leccion)
            .then(function (response) {
                setLoading(false)
                setEdited(false);
                ToastAndroid.show('Cambios Guardados!', ToastAndroid.SHORT);
            })
            .catch(function (error) {
                console.log(error);
                setLoading(false)
                ToastAndroid.show('Error al guardar!', ToastAndroid.SHORT);
            });
    }

    useEffect(() => {
        if (!leccion.actividad && leccionId) {
            getLeccion(leccionId)
                .then(function (response) {
                    const data = response;
                    setLeccion(data);
                    _editor.current.update('Hello\n');
                })
                .catch(function (error) {
                    //console.log(error);
                });
        }
    }, [])

    return (
        <SafeAreaView style={styles.root}>
            <StatusBar style="auto" />

            {loading && <View style={styles.spinner} pointerEvents={'none'}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>}

            {/* si edita se muestra el boton para guardar cambios */}
            {edited && <TouchableOpacity
                style={{
                    position: 'absolute',
                    bottom: 40,
                    right: 0,
                    width: 'auto',
                    padding: 5,
                    zIndex: 1,
                    backgroundColor: '#c3c3c3'
                }}
                onPress={() => updateChanges()}>
                <Image
                    style={{ width: 40, height: 40 }}
                    tintColor='black'
                    source={require('../assets/save.png')}
                />
            </TouchableOpacity>}

            {leccion.actividad && (
                <>
                    <QuillEditor
                        style={styles.editor}
                        placeholder={'Escriba aqui la actividad'}
                        ref={_editor}
                        onHtmlChange={({ html }) => handleChange(html)}
                        initialHtml={leccion.actividad}
                    />
                    <QuillToolbar editor={_editor}
                        options="basic"
                        theme="light"
                    />
                </>
            )}

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
        backgroundColor: '#eaeaea',
    },
    editor: {
        flex: 1,
        padding: 0,
        borderColor: 'gray',
        borderWidth: 1,
        marginHorizontal: 30,
        marginVertical: 5,
        backgroundColor: 'white',
    },
    spinner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'black',
        width,
        height: height - 30,
        opacity: 0.6,
        zIndex: 1,
    },
});