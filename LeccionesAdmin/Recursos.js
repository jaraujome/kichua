import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, Image, FlatList, View, Alert, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import * as DocumentPicker from 'expo-document-picker';
import * as WebBrowser from 'expo-web-browser';

import { apiUrl, updateLeccion, uploadFile } from '../helpers/api';

const { width, height } = Dimensions.get('screen');

export default function Recursos(props) {

    const [leccion, setLeccion] = useState(props.leccion)
    const [fileUploaded, setFileUploaded] = useState(null)

    // selector de archivos
    const pickFile = async () => {
        let result = await DocumentPicker.getDocumentAsync({ type: "*/*", copyToCacheDirectory: true }).then(response => {
            if (response.type == 'success') {
                let { name, size, uri } = response;
                let nameParts = name.split('.');
                let fileType = nameParts[nameParts.length - 1];
                var fileToUpload = {
                    name: name,
                    size: size,
                    uri: uri,
                    type: "application/" + fileType
                };
                setFileUploaded(fileToUpload);
                cargarArchivo();
            }
        });
    }

    // enviar el archivo al backend para guardarlo
    function cargarArchivo() {
        uploadFile(fileUploaded)
            .then(function (response) {
                console.log('response update fileUploaded');
                if (response != 'Error') {
                    Alert.alert('Archivo cargado con exito');
                    let newLeccion = JSON.parse(JSON.stringify(leccion));
                    newLeccion.recursos.push({
                        url: response,
                        name: fileUploaded.name
                    });
                    setLeccion(newLeccion);
                    updateChanges();
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function updateChanges() {
        updateLeccion(leccion)
            .then(function (response) {
                console.log('response update recurso');
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    function changeRecursoName(value, recurso) {
        const index = leccion.recursos.findIndex(r => r._id === recurso._id);
        leccion.recursos.findIndex(recurso);
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.recursos[index].name = value;
        setLeccion(newLeccion);
    }


    function deleteRecurso(recurso) {
        const index = leccion.recursos.findIndex(r => r._id === recurso._id);
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.recursos.splice(index,1);
        setLeccion(newLeccion);
    }

    const confirmDelete = (recurso) =>
        Alert.alert(
            "Eliminar recurso",
            "¿Seguro que desea eliminar el recurso?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => deleteRecurso(recurso) }
            ]
        );

    return (

        <SafeAreaView>
            <FlatList
                data={leccion.recursos}
                keyExtractor={item => `${item._id}`}
                style={{ padding: 15 }}
                renderItem={({ item }) => {
                    return (
                        <View style={{ justifyContent: 'space-around', flexDirection: 'row', alignItems: 'center' }}>

                            <TextInput
                                placeholder="Nombre"
                                value={item.name}
                                style={{ paddingHorizontal: 15, paddingVertical: 5, borderWidth: 1, marginBottom: 10 }}
                                onChangeText={text =>
                                    changeRecursoName(text, item)
                                }
                                onEndEditing={(e) =>
                                    updateChanges()
                                }
                            />

                            <TouchableOpacity
                                onPress={() => WebBrowser.openBrowserAsync(apiUrl + item.url)}>
                                <Image style={{ width: 20, height: 20 }} source={require('../assets/archivo.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => confirmDelete(item)}>
                                <Image style={{ width: 20, height: 20 }} tintColor='red' source={require('../assets/trash.png')} />
                            </TouchableOpacity>
                        </View>
                    );
                }}
            />
            <TouchableOpacity
                style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}
                onPress={() => pickFile()}>
                <Text style={{ paddingVertical: 25 }} > Agregar recurso </Text>
                <Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/add.png')}
                />
            </TouchableOpacity>
        </SafeAreaView>
    );
}