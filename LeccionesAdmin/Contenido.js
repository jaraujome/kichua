import React, { useState } from 'react';
import { Animated, Text, TextInput, Image, View, TouchableOpacity, ScrollView, StyleSheet, ToastAndroid, ActivityIndicator, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Table, Row, Rows } from 'react-native-table-component';
import { updateLeccion } from '../helpers/api';

const { width, height } = Dimensions.get('screen');

const Backdrop = ({ scrollX }) => {
    return (
        <Animated.View
            style={[
                StyleSheet.absoluteFillObject, {
                    backgroundColor: 'white',
                },
            ]}
        />
    )
}

export default function Contenido(props) {

    const scrollX = React.useRef(new Animated.Value(0)).current;
    const flatlistRef = React.useRef();
    const [leccion, setLeccion] = useState(props.leccion)
    const [loading, setLoading] = useState(false)

    const Indicator = ({ scrollX }) => {
        return (
            <View style={{ position: 'absolute', bottom: 10, flexDirection: 'row' }}>
                {leccion.niveles.map((_, i) => {
                    const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
                    const scale = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.8, 1.4, 0.8],
                        extrapolate: 'clamp',
                    });
                    const opacity = scrollX.interpolate({
                        inputRange,
                        outputRange: [0.4, 1.4, 0.4],
                        extrapolate: 'clamp',
                    });
                    return (<Animated.View
                        key={`indicator-${i}`}
                        style={{
                            height: 10,
                            width: 10,
                            borderRadius: 5,
                            backgroundColor: '#1488CC',
                            opacity,
                            margin: 10,
                            transform: [
                                { scale }
                            ]
                        }}
                    />
                    );
                })}
            </View>
        )
    }

    // modificar el nombre del nivel pasado por parametro
    function updateNombreNivel(nombre, nivel) {
        const nivelIndex = nivel.numero - 1
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles[nivelIndex].nombre = nombre;
        setLeccion(newLeccion);
    }

    // agregar un nuevo nivel
    function addNivel() {
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles.push({
            numero: newLeccion.niveles.length + 1,
            nombre: '',
            diccionario: []
        });
        setLeccion(newLeccion);
        updateChanges();
    }

    // eliminar un nivel
    function removerNivel(nivel) {
        const nivelIndex = nivel.numero - 1
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles.splice(nivelIndex, 1);
        newLeccion.niveles = newLeccion.niveles.map((nivel, i) => ({ ...nivel, numero: (i + 1) }));
        setLeccion(newLeccion);
        updateChanges();
    }

    // agregar una nueva parlabra al diccionario del nivel pasado por parametro
    function addWord(nivel) {
        const nivelIndex = nivel.numero - 1;
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles[nivelIndex].diccionario.push({ es: '', kichua: '' });
        setLeccion(newLeccion);
        updateChanges();
    }

    // eliminar una palabra del diccionario
    function removeWord(nivel, index) {
        const nivelIndex = nivel.numero - 1
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles[nivelIndex].diccionario.splice(index, 1);
        setLeccion(newLeccion);
        // flatlistRef.current.scrollToEnd({ animating: true });
        updateChanges();
    }

    // cambiar una palabra del diccionario
    function changeTextNivel(text, lang, index, nivel) {
        const nivelIndex = nivel.numero - 1;
        let newLeccion = JSON.parse(JSON.stringify(leccion));
        newLeccion.niveles[nivelIndex].diccionario[index][lang] = text;
        setLeccion(newLeccion);
    }

    function updateChanges() {
        setLoading(true);
        updateLeccion(leccion)
            .then(function (response) {
                setLoading(false);
                ToastAndroid.show('Cambios Guardados!', ToastAndroid.SHORT);
            })
            .catch(function (error) {
                console.log(error);
                setLoading(false);
                ToastAndroid.show('Error al Guardar!', ToastAndroid.SHORT);
            });
    }

    return (
        <View style={styles.container}>

            {loading && <View style={styles.spinner} pointerEvents={'none'}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>}

            <Backdrop scrollX={scrollX} />

            <Animated.FlatList
                data={leccion.niveles}
                keyExtractor={item => `${item.numero}`}
                horizontal
                scrollEventThrottle={32}
                onScroll={Animated.event(
                    [{ nativeEvent: { contentOffset: { x: scrollX } } }],
                    { useNativeDriver: false }
                )}
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                renderItem={({ item }) => {
                    return (
                        <ScrollView style={{ width, marginBottom: 40 }} ref={flatlistRef}>
                            <LinearGradient
                                colors={['#1488CC', 'white']}
                                style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 15 }}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}>

                                <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center', width }}>
                                    <Text style={{ paddingHorizontal: 15, }} > Nivel {item.numero}</Text>
                                    <TouchableOpacity
                                        onPress={() => removerNivel(item)}>
                                        <Image
                                            style={{ alignSelf: 'flex-end', width: 14, height: 14 }}
                                            source={require('../assets/trash.png')}
                                        />
                                    </TouchableOpacity>
                                </View>


                                <View style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', width }}>
                                    <TouchableOpacity
                                        onPress={() => addWord(item)}>
                                        <View style={{ paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{ paddingVertical: 15 }} > Agregar columna</Text>
                                            <Image
                                                style={{ width: 18, height: 18 }}
                                                source={require('../assets/column.png')}
                                            />
                                        </View>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => addNivel()}>
                                        <View style={{ paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{ paddingVertical: 15 }} > Nivel </Text>

                                            <Image
                                                style={{ width: 14, height: 14 }}
                                                source={require('../assets/add.png')}
                                            />
                                        </View>
                                    </TouchableOpacity>
                                </View>


                                <TextInput
                                    value={item.nombre}
                                    style={styles.name}
                                    placeholder="Titulo del nivel"
                                    onChangeText={text =>
                                        updateNombreNivel(text, item)
                                    }
                                    onEndEditing={(e) =>
                                        updateChanges()
                                    }
                                />
                                {
                                    item.diccionario.map((word, j) => (
                                        <View style={styles.fields} key={`word-${j}`}>
                                            <TextInput
                                                placeholder="es"
                                                value={word.es}
                                                style={styles.word}
                                                onChangeText={text =>
                                                    changeTextNivel(text, 'es', j, item)
                                                }
                                                onEndEditing={(e) =>
                                                    updateChanges()
                                                }
                                            />
                                            <TextInput
                                                placeholder="kichua"
                                                value={word.kichua}
                                                style={styles.word}
                                                onChangeText={text =>
                                                    changeTextNivel(text, 'kichua', j, item)
                                                }
                                                onEndEditing={(e) =>
                                                    updateChanges()
                                                }
                                            />
                                            <TouchableOpacity
                                                onPress={() => removeWord(item, j)}>
                                                <Image
                                                    style={{ width: 14, height: 14, margin: 5 }}
                                                    source={require('../assets/trash.png')}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    ))
                                }
                            </LinearGradient>
                        </ScrollView>
                    );
                }}
            />
            <Indicator scrollX={scrollX} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    list: {
        fontSize: 15,
        padding: 15,
        color: 'white'
    },
    fields: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        margin: 5,
    },
    word: {
        paddingHorizontal: 15,
        borderWidth: 1,
        borderColor: "white",
    },
    name: {
        width,
        paddingHorizontal: 15,
        borderWidth: 1,
        borderColor: "white"
    },
    spinner: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center',
        backgroundColor: 'black',
        width,
        height: height - 30,
        opacity: 0.6,
        zIndex: 1,
    },
});
